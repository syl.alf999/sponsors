import Foundation

enum LoadDataError: Error, Equatable {
    case CustomError
    case NoFile
    case CouldNotLoad
    case CouldNotParse
}


func getErrorMessage(error: Error) -> String {
    if let loadError: LoadDataError = error as? LoadDataError {
        switch loadError {
        case .CouldNotLoad:
            return "Can't load the file"
        case .CouldNotParse:
            return "Can't parse the file"
        case .NoFile:
            return "Can't find the file"
        default:
            return "An error has occured"
        }
    }
    return "An error has occured"
}
