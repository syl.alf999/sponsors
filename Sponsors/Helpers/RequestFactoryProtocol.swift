import Foundation

private let sponsorUrlStr = "https://api.airtable.com/v0/appXKn0DvuHuLw4DV/Sponsors"

struct Response: Codable {
    let id: String
    let deleted: Bool
}
        
struct ErrorResponse: Codable {
    let error: String
}

enum RequestType: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
}

enum CustomError: Error{
    case requestError
    case statusCodeError
    case parsingError
}

// Request Factory
protocol RequestFactoryProtocol {
    func createRequest(urlStr: String, requestType: RequestType, params: [String]?) -> URLRequest
    func getSponsorList(callback: @escaping ((errorType: CustomError?, errorMessage: String?), [Sponsor]?) -> Void)
}

class RequestFactory: RequestFactoryProtocol {
    
    internal func createRequest(urlStr: String, requestType: RequestType, params: [String]?) -> URLRequest {
        var url: URL = URL(string: urlStr)!
        if let params = params {
            var urlParams = urlStr
            for param in params {
                urlParams = urlParams + "/" + param
            }
            print(urlParams)
            url = URL(string: urlParams)!
        }
        var request = URLRequest(url: url)
        request.timeoutInterval = 100
        request.httpMethod = requestType.rawValue
        let accessToken = "keyafnvKkP8QoPE2f"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField:"Authorization")
        return request
    }

    func getSponsorList(callback: @escaping ((errorType: CustomError?, errorMessage: String?), [Sponsor]?) -> Void) {
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: createRequest(urlStr: sponsorUrlStr, requestType: .get, params: nil)) {
            (data, response, error) in if let data = data, error == nil {
                if let responseHttp = response as? HTTPURLResponse {
                    if responseHttp.statusCode == 200 {
                        print(responseHttp.statusCode)
                        if let response = try?JSONDecoder().decode(SponsorRecords.self, from: data) {
                            callback((nil, nil), response.records)
                        }
                        else {
                            callback((CustomError.parsingError, "parsing error"), nil)
                        }
                    }
                    else {
                        callback((CustomError.statusCodeError, "status code: \(responseHttp.statusCode)"), nil)
                    }
                }
                
            }
            else{
                callback((CustomError.requestError, error.debugDescription), nil)
            }
        }
        task.resume()
    }

}
